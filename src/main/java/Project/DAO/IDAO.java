package Project.DAO;

import Project.DtoClasses.Orders;

import java.util.List;

public interface IDAO {

    Orders saveData(Orders order);

    void delete(Long id);

    List<Orders> getAllOrders();

    Orders getOrdrerById(Long id);

}
