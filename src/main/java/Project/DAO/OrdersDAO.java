package Project.DAO;

import Project.DtoClasses.OrderRow;
import Project.DtoClasses.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

@Repository
public class OrdersDAO {

    //@Autowired
    //private JdbcTemplate template;
    //
    //@Override
    //public Orders saveData(Orders order){
    //
    //    String sql1 = "insert into orders(id, orderNumber) values (next value for seq1, ?)";
    //    String sql2 = "insert into orderRows(id, id_row, itemName, quantity, price) values (next value for seq2, ?, ?, ?, ?)";
    //
    //    GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
    //
    //    template.update(conn -> {
    //        PreparedStatement ps = conn.prepareStatement(sql1, new String[]{"id"});
    //        ps.setString(1, order.getOrderNumber());
    //        return ps;
    //    }, keyHolder);
    //
    //    if(order.getOrderRows() != null){
    //        for (OrderRow orderRow: order.getOrderRows()) {
    //            template.update(conn -> {
    //                PreparedStatement ps = conn.prepareStatement(sql2);
    //
    //                ps.setLong(1, keyHolder.getKey().longValue());
    //                ps.setString(2, orderRow.getItemName());
    //                ps.setInt(3, orderRow.getQuantity());
    //                ps.setInt(4, orderRow.getPrice());
    //                return ps;
    //            });
    //
    //        }
    //    }
    //
    //    else {
    //        template.update(conn -> {
    //            PreparedStatement ps = conn.prepareStatement(sql2);
    //            ps.setLong(1, keyHolder.getKey().longValue());
    //            ps.setString(2, "");
    //            ps.setInt(3, 0);
    //            ps.setInt(4, 0);
    //            return ps;
    //        });
    //    }
    //
    //    order.setId(keyHolder.getKey().longValue());
    //
    //    return order;
    //
    //}
    //
    //@Override
    //public void delete(Long id){
    //    String sql1 = "delete from orders where id = ?";
    //    String sql2 = "delete from orderRows where id_row = ?";
    //    template.update(sql1, id);
    //    template.update(sql2, id);
    //}
    //
    //@Override
    //public List<Orders> getAllOrders(){
    //    String sql = "select * from orders left join orderRows on orders.id = orderRows.id_row";
    //    List<Orders> orders = template.query(sql, getOrdersRowMapper());
    //    Long i = 0L;
    //    List<Orders> returnList = new ArrayList<>();
    //
    //    for (Orders order: orders) {
    //        if (!i.equals(order.getId())){
    //            i =  order.getId();
    //            returnList.add(order);
    //        }
    //        else{
    //            returnList.get(returnList.size()-1).addOrderRow(order.getOrderRows().get(0));
    //        }
    //    }
    //
    //    return returnList;
    //}
    //
    //@Override
    //public Orders getOrdrerById(Long id){
    //    String sql = "select * from orders left join orderRows on orders.id = orderRows.id_row where orders.id = ?";
    //    List<Orders> ordersList = new ArrayList<>();
    //    ordersList.add(template.queryForObject(sql, new Object[]{id}, getOrdersRowMapper()));
    //
    //    for (Orders order: ordersList) {
    //        if(ordersList.indexOf(order) == 0)continue;
    //        ordersList.get(0).addOrderRow(order.getOrderRows().get(0));
    //
    //    }
    //
    //    return ordersList.get(0);
    //}
    //
    //private RowMapper<Orders> getOrdersRowMapper() {
    //    return (rs, rowNum)->{
    //        return new Orders(
    //                rs.getLong("id"),
    //                rs.getString("orderNumber"),
    //                new OrderRow(
    //                        rs.getString("itemName"),
    //                        rs.getInt("quantity"),
    //                        rs.getInt("price")
    //                )
    //        );
    //    };
    //}
}
