package Project.controller;

import Project.DtoClasses.ValidationErrors;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ControllerAdvice
public class ErrorController {

    @ResponseBody
    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ValidationErrors argumentNotValid(MethodArgumentNotValidException exeption){
        List<FieldError> errorList = exeption.getBindingResult().getFieldErrors();
        ValidationErrors validError = new ValidationErrors();

        for (FieldError error: errorList ) {
            validError.addError(error.getCode(), error.getDefaultMessage());
        }

    return validError;
    }
}
