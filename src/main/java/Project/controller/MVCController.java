package Project.controller;

import Project.DAO.OrdersDAO;
import Project.DtoClasses.Orders;
import Project.dao.OrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class MVCController {

    @Autowired
    private OrderDao dao;

    @GetMapping("orders")
    public List<Orders> getOrders(){
        return dao.getAllOrders();
    }

    @GetMapping("orders/{id}")
    public Orders getOrderByID(@PathVariable("id") Long id){
        return dao.getOrdrerById(id);
    }

    @DeleteMapping("orders/{id}")
    public void delete(@PathVariable("id") Long id){
      dao.delete(id);
      //return dao.getAllOrders();
    }

    @PostMapping("orders")
    public Orders postOrders(@RequestBody @Valid Orders order){
        return dao.save(order);
    }
}
