package Project.Utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class streamToString {
    public static String toStrin(InputStream stream) {
        BufferedReader buf = new BufferedReader(new InputStreamReader(stream));
        return buf.lines().collect(Collectors.joining("\n"));
    }


}