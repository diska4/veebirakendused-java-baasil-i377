package Project.conf;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@Import(Project.conf.DbConfig.class)
@ComponentScan(basePackages = {"Project.controller"})
public class MvcConfig {


}