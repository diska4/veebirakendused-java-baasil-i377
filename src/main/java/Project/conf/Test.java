package Project.conf;

import Project.DAO.OrdersDAO;
import Project.DtoClasses.OrderRow;
import Project.DtoClasses.Orders;
import Project.dao.OrderDao;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) throws Exception{
        ConfigurableApplicationContext ctx =
                new AnnotationConfigApplicationContext(Project.conf.DbConfig.class);

        OrderDao dao = ctx.getBean(OrderDao.class);
        OrderRow row = new OrderRow("fad", 11, 11);
        OrderRow r = new OrderRow("fjf", 2, 3);
        List<OrderRow> a = new ArrayList<>();
        a.add(row);
        a.add(r);
        System.out.println(new Orders("ABC123", a));
        dao.save(new Orders("ABC123", a));
        System.out.println(dao.getOrdrerById(1L));
        System.out.println(dao.getAllOrders());
        ctx.close();
    }

}
