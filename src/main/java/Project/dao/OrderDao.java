package Project.dao;

import Project.DtoClasses.Orders;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class OrderDao {

    @PersistenceContext
    public EntityManager em;

    @Transactional
    public Orders save(Orders order){
       em.persist(order);
       return order;
    }

    public Orders getOrdrerById(Long id) {
        TypedQuery<Orders> query = em.createQuery("select o from Orders o where o.id = :id", Orders.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }

    public List<Orders> getAllOrders() {
        return em.createQuery("select o from Orders o", Orders.class).getResultList();
    }

    public void delete(Long id) {
        em.createQuery("delete from Orders o where o.id = :id").setParameter("id", id).executeUpdate();

    }
}
