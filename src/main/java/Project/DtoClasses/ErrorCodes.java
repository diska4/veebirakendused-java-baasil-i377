package Project.DtoClasses;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ErrorCodes {



    public String code;

    public List<String> arguments = new ArrayList<>();
}
