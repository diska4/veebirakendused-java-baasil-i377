package Project.DtoClasses;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ValidationErrors {


    public List<ErrorCodes> errors = new ArrayList<>();

    public void addError(String code, String defaultMsg){
        ErrorCodes errorCodes = new ErrorCodes();

        errorCodes.arguments.add(defaultMsg);
        errorCodes.code = code;
        errors.add(errorCodes);
    }
}
