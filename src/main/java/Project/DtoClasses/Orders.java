package Project.DtoClasses;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@Table(name = "orders")
public class Orders {
    public Orders(String orderNumber, List<OrderRow> orderRows) {

        this.orderNumber = orderNumber;
        this.orderRows = orderRows;


    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    @SequenceGenerator(name = "seq", sequenceName = "order_sequence", allocationSize = 1)
    public Long id;

    @Column(name = "order_number")
    public String orderNumber;


    @ElementCollection(fetch =  FetchType.EAGER)
    @CollectionTable(
            name = "order_rows",
            joinColumns =@JoinColumn(name = "orders_id", referencedColumnName = "id")
    )
    public List< OrderRow> orderRows = new ArrayList<>();


}
