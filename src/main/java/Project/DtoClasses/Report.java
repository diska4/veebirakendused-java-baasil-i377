package Project.DtoClasses;

import lombok.Setter;

public class Report {
    @Override
    public String toString() {
        return "Report{" +
                "count=" + count +
                ", turnoverWithoutVAT=" + turnoverWithoutVAT +
                ", turnoverVAT=" + turnoverVAT +
                ", turnoverWithVAT=" + turnoverWithVAT +
                ", averageOrderAmount=" + averageOrderAmount +
                '}';
    }

    @Setter
    public int count;
    @Setter
    public int turnoverWithoutVAT;
    @Setter
    public int turnoverVAT;
    @Setter
    public long turnoverWithVAT;
    @Setter
    public long averageOrderAmount;

}
