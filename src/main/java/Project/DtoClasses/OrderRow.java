package Project.DtoClasses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class OrderRow {

    @Column(name = "item_name")
    private String itemName;

    @Column(name = "quantity")
    @Min(message = "quantity need to be more or eqals than 1", value = 1)
    @NotNull(message = "quantity can not be empty or null")
    public Integer quantity;

    @Column(name = "price")
    @Min(message = "price need to be more or eqals than 1", value = 1)
    @NotNull(message = "price can not be empty or null")
    public Integer price;

}
